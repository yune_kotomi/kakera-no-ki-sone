require 'nokogiri'

doc = Nokogiri::HTML(open(ARGV[0]).read)

link = doc.css('link[rel]').first
css = link.attr('href')
styles = open(File.join(File.dirname(ARGV[0]), css)).read
style = Nokogiri::XML::Element.new('style', doc)
style.add_child(Nokogiri::XML::CDATA.new(doc, styles))
link.remove
doc.css('head').first.add_child(style)

script = doc.css('script[src]').first
js = script.attr('src')
src = open(File.join(File.dirname(ARGV[0]), js)).read
script.add_child(Nokogiri::XML::CDATA.new(doc, src))
script.remove_attribute('src')

open(ARGV[1], 'w') {|f| f.puts doc.to_s }
