import Hatena from '../../../../../vendor/text-hatena.js/text-hatena.js'
const parser = new Hatena.TextHatena({ sectionanchor: '■' })

const app = document.querySelector('#app')
const doc = JSON.parse(document.querySelector('#document-body').value)
const markup = doc.markup

const crEl = (tagName) => {
  return document.createElement(tagName)
}

const tocItem = (src) => {
  const body = crEl('li')
  const title = crEl('div')
  body.appendChild(title)
  title.innerText = src.title

  if (src.children.length > 0) {
    const children = crEl('ol')
    body.appendChild(children)

    src.children.forEach((c) => {
      const child = tocItem(c)
      children.appendChild(child)
    })
  }

  return body
}

const leafItem = (src) => {
  const container = crEl('div')
  container.className = 'leaf'

  const title = crEl('h2')
  container.appendChild(title)
  title.innerText = src.title

  const body = crEl('div')
  container.appendChild(body)
  switch (markup) {
    case 'hatena':
      body.innerHTML = parser.parse(src.body)
      break

    default:
      // Markdownの展開はサポートしない
      body.innerText = src.body
  }

  if (src.children.length > 0) {
    const children = crEl('ol')
    container.appendChild(children)

    src.children.forEach((c) => children.appendChild(leafItem(c)))
  }

  return container
}

const toc = crEl('ul')
toc.appendChild(tocItem(doc))
app.appendChild(toc)

const contents = crEl('div')
contents.appendChild(leafItem(doc))
app.appendChild(contents)
