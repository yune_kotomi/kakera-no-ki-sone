/**
 * @jest-environment jsdom
 */

import React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { Provider } from 'react-redux'
import * as ReactRedux from 'react-redux'

import { store } from '../slice/store'
import Contents from './Contents'

const setMockState = (params) => {
  const { selected, editable } = params

  const mockState = {
    document: {
      leaves: {
        root: {
          id: 'root',
          title: 'rt',
          body: '',
          open: true,
          children: [],
        },
      },
    },
    editor: {
      selected: selected,
      editable: editable,
    },
  }

  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementation((selector) => selector(mockState))
}

const renderComponent = () => {
  render(
    <Provider store={store}>
      <Contents id="root" />
    </Provider>
  )
}

describe('Contents', () => {
  it('should be rendered', () => {
    setMockState({})
    renderComponent()

    const title = screen.getByLabelText('content-title')
    expect(title.innerHTML).toEqual(' rt')
  })
})
