/**
 * @jest-environment jsdom
 */

import React from 'react'
import {
  render,
  screen,
  TestingLibraryElementError,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import EditForm from './EditForm'

import * as Authentication from '../../../../../authentication.js'
jest.spyOn(Authentication, 'refreshToken').mockImplementation(() => jest.fn())

describe('EditForm', () => {
  const title = 'title1'
  const body = 'body1'
  const onSubmit = jest.fn()
  const onClose = jest.fn()

  beforeEach(() => {
    render(
      <EditForm
        title={title}
        body={body}
        onSubmit={onSubmit}
        onClose={onClose}
      />
    )
  })

  afterEach(() => jest.clearAllMocks())

  it('与えられたタイトル・本文を初期値として表示', () => {
    const titleField = screen
      .getByLabelText('title-input')
      .querySelector('input')
    expect(titleField.value).toEqual(title)
    const bodyField = screen
      .getByLabelText('body-input')
      .querySelector('textarea')
    expect(bodyField.value).toEqual(body)
  })

  describe('確定ボタン', () => {
    it('クリックでonSubmitへ入力内容を引き渡し、onCloseを発火', async () => {
      const button = screen.getByLabelText('submit')
      await userEvent.click(button)

      expect(onSubmit).toHaveBeenCalledWith({ title: title, body: body })
      expect(onClose).toHaveBeenCalled()
    })
  })

  describe('キャンセルボタン', () => {
    it('クリックでonCloseを発火', async () => {
      const button = screen.getByLabelText('cancel')
      await userEvent.click(button)

      expect(onClose).toHaveBeenCalled()
    })
  })
})
