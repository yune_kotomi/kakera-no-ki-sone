/**
 * @jest-environment jsdom
 */

import React from 'react'
import {
  render,
  screen,
  TestingLibraryElementError,
} from '@testing-library/react'

import Presenter from './Presenter'

describe('Presenter', () => {
  describe('markupがplaintextの場合', () => {
    it('プレーンテキストとして出力される', () => {
      const markup = 'plaintext'
      const value = 'text1\ntext2'
      const { container } = render(<Presenter markup={markup} value={value} />)

      const actual = container.querySelector('.plaintext.body')
      expect(actual.innerHTML).toEqual('text1<br>text2')
    })
  })

  describe('markupがhatenaの場合', () => {
    it('はてな記法が展開される', () => {
      const markup = 'hatena'
      const value = '+ text1\n+ text2'
      const { container } = render(<Presenter markup={markup} value={value} />)

      const actual = container.querySelector('.hatena.body')
      expect(actual.innerHTML.replace(/[\s\r]/g, '')).toEqual(
        '<divclass="section"><ol><li>text1</li><li>text2</li></ol></div>'
      )
    })
  })

  describe('markupがmarkdownの場合', () => {
    it('markdownとして展開される', () => {
      const markup = 'markdown'
      const value = '+ text1\n+ text2'
      const { container } = render(<Presenter markup={markup} value={value} />)

      const actual = container.querySelector('.markdown.body')
      expect(actual.innerHTML.replace(/[\s\r]/g, '')).toEqual(
        '<ul><li>text1</li><li>text2</li></ul>'
      )
    })
  })

  describe('不明なmarkupの場合', () => {
    it('plaintextとして出力される', () => {
      const markup = 'hoge'
      const value = 'text1\ntext2'
      const { container } = render(<Presenter markup={markup} value={value} />)

      const actual = container.querySelector('.plaintext.body')
      expect(actual.innerHTML).toEqual('text1<br>text2')
    })
  })
})
