/**
 * @jest-environment jsdom
 */

import React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { Provider } from 'react-redux'
import * as ReactRedux from 'react-redux'

import { store } from '../slice/store'
import { documentSlice } from '../slice/documentSlice'
import { editorSlice } from '../slice/editorSlice'
import Content from './Content'

import * as Authentication from '../../../../../authentication.js'
jest.spyOn(Authentication, 'refreshToken').mockImplementation(() => jest.fn())

const dispatch = jest.fn()
jest.spyOn(ReactRedux, 'useDispatch').mockImplementation(() => dispatch)

const dActions = documentSlice.actions
const eActions = editorSlice.actions

const setMockState = (params) => {
  const { selected, editable } = params

  const mockState = {
    document: {
      leaves: {
        root: {
          id: 'root',
          title: 'rt',
          open: true,
          children: ['child1'],
        },
        child1: {
          id: 'child1',
          title: 'c1',
          body: '',
          open: true,
          parent: 'root',
          children: ['child11'],
        },
        child11: {
          id: 'child11',
          title: 'c11',
          body: '',
          open: true,
          parent: 'child1',
          children: [],
        },
      },
    },
    editor: {
      selected: selected,
      editable: editable,
    },
  }

  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementation((selector) => selector(mockState))
}

const renderComponent = (id) => {
  render(
    <Provider store={store}>
      <Content chapterPrefix="1.2" id={id} />
    </Provider>
  )
}

describe('Content', () => {
  afterEach(() => jest.clearAllMocks())

  describe('initial render', () => {
    it('章番号付きでタイトルを表示', async () => {
      setMockState({})
      renderComponent('child1')

      const titles = screen.getAllByLabelText('content-title')
      expect(titles.map((t) => t.innerHTML)).toEqual([
        '1.2.1 c1',
        '1.2.1.1 c11',
      ])
    })

    it('編集状態だとフォームを表示', async () => {
      setMockState({ editable: 'child11' })
      renderComponent('child11')

      screen.getByLabelText('title-input')
    })
  })

  describe('編集操作', () => {
    it('クリックで編集状態へ', async () => {
      setMockState({})
      renderComponent('child11')

      const clickTarget = screen.getByLabelText('content')
      await userEvent.click(clickTarget)

      expect(dispatch).toHaveBeenNthCalledWith(
        1,
        eActions.enterEdit({ id: 'child11' })
      )
      expect(dispatch).toHaveBeenNthCalledWith(
        2,
        eActions.select({ id: 'child11' })
      )
    })

    it('フォームの確定で更新して編集状態を解除', async () => {
      setMockState({ editable: 'child11' })
      renderComponent('child11')

      const titleField = screen
        .getByLabelText('title-input')
        .querySelector('input')
      const bodyField = screen
        .getByLabelText('body-input')
        .querySelector('textarea')

      await userEvent.type(titleField, 'new')
      await userEvent.type(bodyField, 'new')

      const button = screen.getByLabelText('submit')
      await userEvent.click(button)

      expect(dispatch).toHaveBeenNthCalledWith(
        1,
        dActions.update({
          id: 'child11',
          values: { title: 'c11new', body: 'new' },
        })
      )
      expect(dispatch).toHaveBeenNthCalledWith(2, eActions.leaveEdit())
    })
  })
})
