/**
 * @jest-environment jsdom
 */

import React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { Provider } from 'react-redux'
import * as ReactRedux from 'react-redux'

import { store } from '../slice/store'
import { documentSlice } from '../slice/documentSlice'
import { editorSlice } from '../slice/editorSlice'
import Leaf from './Leaf'

const dispatch = jest.fn()
jest.spyOn(ReactRedux, 'useDispatch').mockImplementation(() => dispatch)

const dActions = documentSlice.actions
const eActions = editorSlice.actions

const setMockState = (params) => {
  const { open, selected } = params

  const mockState = {
    document: {
      leaves: {
        root: {
          id: 'root',
          title: 'rt',
          open: true,
          children: ['child1', 'child2'],
        },
        child1: {
          id: 'child1',
          title: 'c1',
          open: open,
          parent: 'root',
          children: ['child11'],
        },
        child11: {
          id: 'child11',
          title: 'c11',
          open: true,
          parent: 'child1',
          children: [],
        },
        child2: {
          id: 'child2',
          open: true,
          parent: 'root',
          children: ['child21', 'child22', 'child23'],
        },
        child21: {
          id: 'child21',
          open: true,
          parent: 'child2',
          children: [],
        },
        child22: {
          id: 'child22',
          open: true,
          parent: 'child2',
          children: [],
        },
        child23: {
          id: 'child23',
          open: true,
          parent: 'child2',
          children: [],
        },
      },
    },
    editor: {
      selected: selected,
    },
  }

  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementation((selector) => selector(mockState))
}

const renderComponent = (id) => {
  render(
    <Provider store={store}>
      <Leaf chapterPrefix="1.2" id={id} />
    </Provider>
  )
}

describe('Leaf', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('initial render', () => {
    describe('開いている場合', () => {
      beforeEach(() => {
        setMockState({ open: true })
        renderComponent('child1')
      })

      it('親子の章番号とタイトルが表示される', () => {
        const titles = screen
          .getAllByLabelText('leaf-title')
          .map((e) => e.innerHTML)
        expect(titles).toEqual(['1.2.1 c1', '1.2.1.1 c11'])
      })

      describe('閉じるボタン', () => {
        it('表示', () => {
          const buttons = screen.getAllByLabelText('collapse-button')
          expect(buttons.length).toEqual(2)
        })

        it('クリックで更新 open: false', async () => {
          const button = screen.getAllByLabelText('collapse-button')[0]
          const action = dActions.update({
            id: 'child1',
            values: { open: false },
          })
          await userEvent.click(button)
          expect(dispatch).toHaveBeenCalledWith(action)
        })
      })
    })

    describe('閉じている場合', () => {
      beforeEach(() => {
        setMockState({ open: false })
        renderComponent('child1')
      })

      it('親の章番号とタイトルのみ表示される', () => {
        const titles = screen
          .getAllByLabelText('leaf-title')
          .map((e) => e.innerHTML)
        expect(titles).toEqual(['1.2.1 c1'])
      })

      describe('開くボタン', () => {
        it('開くボタンを表示', () => {
          screen.getByLabelText('expand-button')
        })

        it('クリックで更新 open: true', async () => {
          const button = screen.getByLabelText('expand-button')
          const action = dActions.update({
            id: 'child1',
            values: { open: true },
          })
          await userEvent.click(button)
          expect(dispatch).toHaveBeenCalledWith(action)
        })
      })
    })
  })

  describe('選択状態', () => {
    it('選択されているとclassName=selected', () => {
      setMockState({ open: true, selected: 'child1' })
      renderComponent('child1')
      const container = screen.getAllByLabelText('leaf-container')[0]
      expect(container.className).toEqual('selected')
    })

    it('選択されていなければclassNameはカラ', () => {
      setMockState({ open: true, selected: '' })
      renderComponent('child1')
      const container = screen.getAllByLabelText('leaf-container')[0]
      expect(container.className).toEqual('')
    })
  })

  describe('ボタン類', () => {
    describe('タイトル', () => {
      it('クリックで選択状態', async () => {
        setMockState({ open: true })
        renderComponent('child1')

        const title = screen.getAllByLabelText('leaf-title')[0]
        const action = eActions.select({ id: 'child1' })
        await userEvent.click(title)
        expect(dispatch).toHaveBeenCalledWith(action)
      })

      it('選択後2回目のクリックでモバイル本文表示', async () => {
        setMockState({ open: true, selected: 'child1' })
        renderComponent('child1')

        const title = screen.getAllByLabelText('leaf-title')[0]
        const action = eActions.setMobileEditing(true)
        await userEvent.click(title)
        expect(dispatch).toHaveBeenCalledWith(action)
      })
    })

    describe('削除', () => {
      beforeEach(() => {
        setMockState({ open: true, selected: 'child1' })
        renderComponent('child1')
      })

      it('選択状態でのみ表示', () => {
        screen.getByLabelText('remove-button')
      })

      it('クリックで削除', async () => {
        const action = dActions.remove({ id: 'child1' })

        const button = screen.getByLabelText('remove-button')
        await userEvent.click(button)
        expect(dispatch).toHaveBeenCalledWith(action)
      })
    })

    describe('←', () => {
      it('ルート直下だと表示されない', () => {
        setMockState({ selected: 'child1' })
        renderComponent('child1')
        const button = screen.queryByLabelText('to-up-button')
        expect(button).toEqual(null)
      })

      it('ルート直下でなければ表示', () => {
        setMockState({ selected: 'child11' })
        renderComponent('child11')
        screen.getByLabelText('to-up-button')
      })

      it('クリックで親の直後に移動', async () => {
        setMockState({ selected: 'child11' })
        renderComponent('child11')
        const button = screen.getByLabelText('to-up-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.move({ id: 'child11', destinationId: 'root', position: 1 })
        )
      })
    })

    describe('↑', () => {
      it('最初の子には表示されない', () => {
        setMockState({ selected: 'child1' })
        renderComponent('child1')
        const button = screen.queryByLabelText('to-elder-button')
        expect(button).toEqual(null)
      })

      it('中間の子には表示される', () => {
        setMockState({ selected: 'child2' })
        renderComponent('child2')
        screen.getByLabelText('to-elder-button')
      })

      it('クリックで直前の葉と入れ替える', async () => {
        setMockState({ selected: 'child2' })
        renderComponent('child2')
        const button = screen.getByLabelText('to-elder-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.move({ id: 'child2', destinationId: 'root', position: 0 })
        )
      })
    })

    describe('→', () => {
      it('最初の子には表示されない', () => {
        setMockState({ selected: 'child21' })
        renderComponent('child21')
        const button = screen.queryByLabelText('to-down-button')
        expect(button).toEqual(null)
      })

      it('二番目以降の子には表示される', () => {
        setMockState({ selected: 'child22' })
        renderComponent('child22')
        screen.getByLabelText('to-down-button')
      })

      it('クリックで、直前の葉に子がなければ最初の子にする', async () => {
        setMockState({ selected: 'child22' })
        renderComponent('child22')
        const button = screen.getByLabelText('to-down-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.move({
            id: 'child22',
            destinationId: 'child21',
            position: 0,
          })
        )
      })

      it('直前の葉に子が存在する場合、子リストの末尾に挿入する', async () => {
        setMockState({ selected: 'child2' })
        renderComponent('child2')
        const button = screen.getByLabelText('to-down-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.move({
            id: 'child2',
            destinationId: 'child1',
            position: 1,
          })
        )
      })
    })

    describe('↓', () => {
      it('最後の子には表示されない', () => {
        setMockState({ selected: 'child23' })
        renderComponent('child23')
        const button = screen.queryByLabelText('to-younger-button')
        expect(button).toEqual(null)
      })

      it('最後以外の子には表示される', () => {
        setMockState({ selected: 'child22' })
        renderComponent('child22')
        screen.getByLabelText('to-younger-button')
      })

      it('クリックで直後の子と入れ替える', async () => {
        setMockState({ selected: 'child22' })
        renderComponent('child22')
        const button = screen.getByLabelText('to-younger-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.move({
            id: 'child22',
            destinationId: 'child2',
            position: 2,
          })
        )
      })
    })

    describe('+', () => {
      it('クリックで子を新規追加 挿入位置は先頭', async () => {
        setMockState({ selected: 'child2' })
        renderComponent('child2')
        const button = screen.getByLabelText('add-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.add({
            parentId: 'child2',
            position: 0,
            child: {
              id: expect.anything(),
              title: '',
              body: '',
              open: true,
              parent: 'child2',
              children: [],
            },
          })
        )
      })
    })
  })
})
