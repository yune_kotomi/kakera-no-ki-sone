/**
 * @jest-environment jsdom
 */

import React from 'react'
import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { Provider } from 'react-redux'
import * as ReactRedux from 'react-redux'

// react-scroll@1.8.3以降でテスト中にscrollTo()を叩くと死ぬ
import { scroller } from 'react-scroll'
jest.spyOn(scroller, 'scrollTo').mockImplementation(jest.fn())

import { store } from '../slice/store'
import { documentSlice } from '../slice/documentSlice'
import { editorSlice } from '../slice/editorSlice'

import Tree from './Tree'

const dispatch = jest.fn()
jest.spyOn(ReactRedux, 'useDispatch').mockImplementation(() => dispatch)

const dActions = documentSlice.actions
const eActions = editorSlice.actions

const setMockState = (params) => {
  const { selected } = params

  const mockState = {
    document: {
      leaves: {
        root: {
          id: 'root',
          title: 'rt',
          open: true,
          children: ['child1'],
        },
        child1: {
          id: 'child1',
          title: 'c1',
          open: true,
          parent: 'root',
          children: [],
        },
      },
    },
    editor: {
      selected: selected,
    },
  }

  jest
    .spyOn(ReactRedux, 'useSelector')
    .mockImplementation((selector) => selector(mockState))
}

const renderComponent = () => {
  render(
    <Provider store={store}>
      <Tree id="root" />
    </Provider>
  )
}

describe('Tree', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('initial render', () => {
    it('rootと子を表示する', async () => {
      setMockState({})
      renderComponent()

      const title = screen.getByLabelText('root-title')
      expect(title.innerHTML).toEqual('rt')

      const child1 = screen.getByLabelText('leaf-title')
      expect(child1.innerHTML).toEqual('1 c1')
    })
  })

  describe('klass', () => {
    it('選択時はklass=selected', async () => {
      setMockState({ selected: 'root' })
      renderComponent()
      const elem = screen.getByLabelText('root-leaf')
      expect(elem.className).toEqual('root-selected')
    })

    it('非選択時はklassなし', async () => {
      setMockState({})
      renderComponent()
      const elem = screen.getByLabelText('root-leaf')
      expect(elem.className).toEqual('')
    })
  })

  describe('タイトル', () => {
    it('非選択時にクリックで選択', async () => {
      setMockState({})
      renderComponent()
      const title = screen.getByLabelText('root-title')
      await userEvent.click(title)
      expect(dispatch).toHaveBeenCalledWith(eActions.select({ id: 'root' }))
    })

    it('選択時にもう一度クリックでmobileEditing=true', async () => {
      setMockState({ selected: 'root' })
      renderComponent()
      const title = screen.getByLabelText('root-title')
      await userEvent.click(title)
      expect(dispatch).toHaveBeenCalledWith(eActions.setMobileEditing(true))
    })
  })

  describe('追加ボタン', () => {
    it('非選択時は表示しない', async () => {
      setMockState({})
      renderComponent()
      const button = screen.queryByLabelText('add-button')
      expect(button).toEqual(null)
    })

    describe('選択時', () => {
      beforeEach(() => {
        setMockState({ selected: 'root' })
        renderComponent()
      })

      it('表示する', async () => {
        screen.getByLabelText('add-button')
      })

      it('クリックで子リストの先頭に子を追加', async () => {
        const button = screen.getByLabelText('add-button')
        await userEvent.click(button)

        expect(dispatch).toHaveBeenCalledWith(
          dActions.add({
            parentId: 'root',
            position: 0,
            child: {
              id: expect.anything(),
              title: '',
              body: '',
              open: true,
              parent: 'root',
              children: [],
            },
          })
        )
      })
    })
  })
})
