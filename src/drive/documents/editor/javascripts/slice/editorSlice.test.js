import { editorSlice } from './editorSlice'

const reducer = editorSlice.reducer
const actions = editorSlice.actions

describe('editor slice', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      selected: null,
      editable: null,
      documentListUrl: '',
      syncStatus: 'not_changed',
      mobileEditing: false,
    })
  })

  describe('enterEdit', () => {
    it('編集中の葉IDをセットする', () => {
      const action = actions.enterEdit({ id: 'edit-id' })
      const actual = reducer(undefined, action)
      expect(actual.editable).toEqual('edit-id')
    })
  })

  describe('leaveEdit', () => {
    it('編集中IDをクリアする', () => {
      const action = actions.leaveEdit()
      const actual = reducer({ editable: 'edit-id' }, action)
      expect(actual.editable).toEqual(null)
    })
  })

  describe('select', () => {
    it('選択中IDをセットする', () => {
      const action = actions.select({ id: 'id' })
      const actual = reducer(undefined, action)
      expect(actual.selected).toEqual('id')
    })
  })

  describe('setDocumentListUrl', () => {
    it('文書リストURLをセット', () => {
      const action = actions.setDocumentListUrl('url')
      const actual = reducer(undefined, action)
      expect(actual.documentListUrl).toEqual('url')
    })
  })

  describe('setSyncStatus', () => {
    it('同期ステータスをセット', () => {
      ;['unsynced', 'synced', 'syncing', 'error'].map((stat) => {
        const action = actions.setSyncStatus(stat)
        const actual = reducer(undefined, action)
        expect(actual.syncStatus).toEqual(stat)
      })
    })

    it('不正な動機ステータスはセットできない', () => {
      const action = actions.setSyncStatus('hogehoge')
      const actual = reducer(undefined, action)
      expect(actual.syncStatus).toEqual('not_changed')
    })
  })

  describe('setMobileEditing', () => {
    it('モバイル編集中フラグをセット', () => {
      const action = actions.setMobileEditing(true)
      const actual = reducer(undefined, action)
      expect(actual.mobileEditing).toEqual(true)
    })
  })
})
