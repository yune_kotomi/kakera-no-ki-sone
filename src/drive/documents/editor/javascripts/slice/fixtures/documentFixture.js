export const documentFixture = {
  id: '85eb3824-b21c-4bd4-9f23-8ce8113a3d80',
  title: 'テスト用',
  body: 'root body',
  children: [
    {
      id: 'fb0b2573-7b6d-4cf5-9678-997720cd8d68',
      title: '1 title',
      body: '1 body',
      children: [
        {
          id: 'a92e0fd6-0673-4867-b16a-572a2a318992',
          title: '1-1 title',
          body: '1-1 body',
          children: [],
          open: true,
        },
        {
          id: '9bbe93fb-b055-4eb4-86aa-dfd0412abbf6',
          title: '1-2 title',
          body: '1-2 body',
          children: [],
          open: true,
        },
      ],
      open: true,
    },
    {
      id: 'ced0e990-66fb-40df-bbd9-e30c34da9662',
      title: '2 title',
      body: '2 body',
      children: [
        {
          id: '4be87d55-bf30-447d-90d9-aa7167134353',
          title: '2-1 title',
          body: '2-1 body',
          children: [],
          open: true,
        },
        {
          id: 'ced0efe1-c4b9-4f70-9fe5-d6d586b890bc',
          title: '2-2 title',
          body: '2-2 body',
          children: [
            {
              id: '5e28e439-ac3d-4a88-a685-52b6938bf1dc',
              title: '2-2-1 title',
              body: '2-2-1 body',
              children: [],
              open: true,
            },
          ],
          open: true,
        },
      ],
      open: true,
    },
  ],
  open: true,
  markup: 'hatena',
  version: 1622205762663,
  schema_version: 2,
}
