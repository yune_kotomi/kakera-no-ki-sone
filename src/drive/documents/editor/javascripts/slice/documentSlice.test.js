import { documentSlice, storeToDocument } from './documentSlice'
import { documentFixture } from './fixtures/documentFixture'

const reducer = documentSlice.reducer
const actions = documentSlice.actions

const root = {
  id: 'root',
  title: 'root',
  body: 'root',
  parent: null,
  open: true,
  children: ['child1'],
}
const child1 = {
  id: 'child1',
  title: 'child1',
  body: 'c1',
  parent: 'root',
  open: true,
  children: ['child1-1', 'child1-2'],
}
const child1_1 = {
  id: 'child1-1',
  title: 'child1-1',
  body: 'c1-1',
  parent: 'child1',
  open: true,
  children: [],
}
const child1_2 = {
  id: 'child1-2',
  title: 'child1-2',
  body: 'c1-2',
  parent: 'child1',
  open: true,
  children: [],
}
const newChild = {
  id: 'newchild',
  title: 'newchild',
  body: 'c',
  open: true,
  children: [],
}
const istate = {
  leaves: {
    root: root,
    child1: child1,
    'child1-1': child1_1,
    'child1-2': child1_2,
  },
  root: 'root',
  version: 0,
  markup: 'plaintext',
}

describe('document slice', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      leaves: {},
      root: '',
      version: 0,
      markup: 'plaintext',
    })
  })

  describe('setInitialDocument', () => {
    it('should parse document source', () => {
      const f = documentFixture
      const action = actions.setInitialDocument(f)
      const actual = reducer(undefined, action)

      expect(actual).toEqual({
        leaves: {
          '85eb3824-b21c-4bd4-9f23-8ce8113a3d80': {
            id: '85eb3824-b21c-4bd4-9f23-8ce8113a3d80',
            title: f.title,
            body: f.body,
            open: f.open,
            parent: null,
            children: f.children.map((c) => c.id),
          },
          'fb0b2573-7b6d-4cf5-9678-997720cd8d68': {
            id: 'fb0b2573-7b6d-4cf5-9678-997720cd8d68',
            title: '1 title',
            body: '1 body',
            open: true,
            parent: '85eb3824-b21c-4bd4-9f23-8ce8113a3d80',
            children: [
              'a92e0fd6-0673-4867-b16a-572a2a318992',
              '9bbe93fb-b055-4eb4-86aa-dfd0412abbf6',
            ],
          },
          'a92e0fd6-0673-4867-b16a-572a2a318992': {
            id: 'a92e0fd6-0673-4867-b16a-572a2a318992',
            title: '1-1 title',
            body: '1-1 body',
            open: true,
            parent: 'fb0b2573-7b6d-4cf5-9678-997720cd8d68',
            children: [],
          },
          '9bbe93fb-b055-4eb4-86aa-dfd0412abbf6': {
            id: '9bbe93fb-b055-4eb4-86aa-dfd0412abbf6',
            title: '1-2 title',
            body: '1-2 body',
            open: true,
            parent: 'fb0b2573-7b6d-4cf5-9678-997720cd8d68',
            children: [],
          },
          'ced0e990-66fb-40df-bbd9-e30c34da9662': {
            id: 'ced0e990-66fb-40df-bbd9-e30c34da9662',
            title: '2 title',
            body: '2 body',
            open: true,
            parent: '85eb3824-b21c-4bd4-9f23-8ce8113a3d80',
            children: [
              '4be87d55-bf30-447d-90d9-aa7167134353',
              'ced0efe1-c4b9-4f70-9fe5-d6d586b890bc',
            ],
          },
          '4be87d55-bf30-447d-90d9-aa7167134353': {
            id: '4be87d55-bf30-447d-90d9-aa7167134353',
            title: '2-1 title',
            body: '2-1 body',
            open: true,
            parent: 'ced0e990-66fb-40df-bbd9-e30c34da9662',
            children: [],
          },
          'ced0efe1-c4b9-4f70-9fe5-d6d586b890bc': {
            id: 'ced0efe1-c4b9-4f70-9fe5-d6d586b890bc',
            title: '2-2 title',
            body: '2-2 body',
            open: true,
            parent: 'ced0e990-66fb-40df-bbd9-e30c34da9662',
            children: ['5e28e439-ac3d-4a88-a685-52b6938bf1dc'],
          },
          '5e28e439-ac3d-4a88-a685-52b6938bf1dc': {
            id: '5e28e439-ac3d-4a88-a685-52b6938bf1dc',
            title: '2-2-1 title',
            body: '2-2-1 body',
            open: true,
            parent: 'ced0efe1-c4b9-4f70-9fe5-d6d586b890bc',
            children: [],
          },
        },
        root: '85eb3824-b21c-4bd4-9f23-8ce8113a3d80',
        markup: f.markup,
        version: f.version,
      })
    })
  })

  describe('add', () => {
    it('ルート直下の指定位置に子を挿入できる', () => {
      const action = actions.add({
        parentId: 'root',
        position: 0,
        child: newChild,
      })
      const actual = reducer(istate, action).leaves

      expect(actual['root'].children).toEqual(['newchild', 'child1'])
      expect(actual['newchild'].parent).toEqual('root')
    })

    it('子の指定位置に孫を挿入できる', () => {
      const action = actions.add({
        parentId: 'child1',
        position: 1,
        child: newChild,
      })
      const actual = reducer(istate, action).leaves
      expect(actual['child1'].children).toEqual([
        'child1-1',
        'newchild',
        'child1-2',
      ])
      expect(actual['newchild'].parent).toEqual('child1')
    })
  })

  describe('updateMeta', () => {
    it('versionを更新できる', () => {
      const action = actions.updateMeta({ version: 21 })
      const actual = reducer(istate, action)
      expect(actual.version).toEqual(21)
    })

    it('markupを更新できる', () => {
      const action = actions.updateMeta({ markup: 'hatena' })
      const actual = reducer(istate, action)
      expect(actual.markup).toEqual('hatena')
    })
  })

  describe('update', () => {
    it('ルートを更新できる', () => {
      const action = actions.update({
        id: 'root',
        values: { title: 'new title', body: 'new body' },
      })
      const actual = reducer(istate, action).leaves.root
      expect(actual.title).toEqual('new title')
      expect(actual.body).toEqual('new body')
    })
    it('子を更新できる', () => {
      const action = actions.update({
        id: 'child1',
        values: { title: 'new title', body: 'new body' },
      })
      const actual = reducer(istate, action).leaves.child1
      expect(actual.title).toEqual('new title')
      expect(actual.body).toEqual('new body')
    })
  })

  describe('move', () => {
    it('指定した葉を指定位置に移動できる', () => {
      const action = actions.move({
        id: 'child1-2',
        destinationId: 'root',
        position: 1,
      })
      const actual = reducer(istate, action).leaves
      expect(actual.root.children).toEqual(['child1', 'child1-2'])
      expect(actual.child1.children).toEqual(['child1-1'])
      expect(actual['child1-2'].parent).toEqual('root')
    })
  })

  describe('remove', () => {
    it('指定した葉を削除できる', () => {
      const action = actions.remove({ id: 'child1-2' })
      const actual = reducer(istate, action).leaves
      expect(actual.child1.children).toEqual(['child1-1'])
      expect(actual['child1-2']).toEqual(undefined)
    })

    it('葉が子を持っている場合全て削除される', () => {
      const action = actions.remove({ id: 'child1' })
      const actual = reducer(istate, action).leaves
      expect(actual.root.children).toEqual([])
      expect(actual.child1).toEqual(undefined)
      expect(actual['child1-1']).toEqual(undefined)
      expect(actual['child1-2']).toEqual(undefined)
    })
  })
})

describe('utils', () => {
  it('storeの内容から文書を復元できる', () => {
    const store = reducer(
      undefined,
      actions.setInitialDocument(documentFixture)
    )
    const actual = storeToDocument(store)

    expect(actual).toEqual(documentFixture)
  })
})
