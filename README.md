「カケラの樹」サービス基盤 "Yoichi"
====

「カケラの樹」( http://kakera.yumenosora.net/ )のサービス提供のためのアプリケーションです。

This program is free software. You can use, modify and redistribute it under the conditions of the GNU Affero General Public License. See COPYING for details.
